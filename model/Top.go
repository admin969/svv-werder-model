package model

import (
	"time"
)

const TOP_ENTITY_NAME = "Top"

type Top struct {
	TOLFDNR              int
	VOLFDNR              int
	SILFDNR              int
	Index                int
	Nr                   string
	Betreff              string `datastore:",noindex"`
	Protokoll            string `datastore:",noindex"`
	ProtokollRe          string `datastore:",noindex"`
	AbstimmungZustimmung int
	AbstimmungAblehnung  int
	AbstimmungEnthaltung int
	Beschluss            string `datastore:",noindex"`
	Beschlussart         string
	Status               string
	BSVV                 string
	Federfuehrend        string
	Bearbeiter           string
	Datum                time.Time
	Wochentag            string
	Zeit                 string
	Gremium              string
	Raum                 string
	Ort                  string
	SavedAt              time.Time
}
