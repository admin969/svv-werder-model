package model

import (
	"time"
)

const VORLAGE_ENTITY_NAME = "Vorlage"
const BERATUNG_ENTITY_NAME = "Beratung"

type Beratung struct {
	Status        string
	Typ           string
	BSVV          string
	Datum         time.Time
	SILFDNR       int
	Beschlussart  string
	Gremium       string
	Index         int
	TOLFDNR       int
	Federfuehrend string
	VOLFDNR       int
	Oeff          string

	SavedAt time.Time
}

type Vorlage struct {
	VOLFDNR                     int
	BSVV                        string
	Betreff                     string `datastore:",noindex"`
	Oeff                        string
	Federfuehrend               string
	Bearbeiter                  string
	BeschlussVorlage            string `datastore:",noindex"`
	Begruendung                 string `datastore:",noindex"`
	FinanzielleAuswirkung       string `datastore:",noindex"`
	DatumAngelegt               time.Time
	SavedAt                     time.Time
	LeterBeratungsStatus        string
	LeterBeratungsTyp           string
	LetesBeratungsGremium       string
	LetzteBeratungsSitzung      int
	LetzterBeratungsTop         int
	LetzterBeratungBeschlussart string
	LetzteBeratungDatum         time.Time
	BezueglichVOLFDNR           int
	BezueglichBSVV              string
}
