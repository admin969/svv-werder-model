package model

import (
	"time"
)

const SITZUNG_ENTITY_NAME = "Sitzung"

type Sitzung struct {
	SILFDNR    int
	Datum      time.Time
	Gremium    string
	Einreicher string
	Status     string
	Title      string
	Wochentag  string
	Uhrzeit    string
	Raum       string
	Ort        string
	SavedAt    time.Time
}
