package model

import "time"

const ANLAGE_ENTITY_NAME = "Anlage"

type Anlage struct {
	Title       string
	Name        string
	Url         string
	Description string
	ContentType string
	Error       string
	Size        string
	SavedAt     time.Time
}
