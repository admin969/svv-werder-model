package model

import (
	"time"
)

const ANLAGEDOCUMENT_ENTITY_NAME = "BasisAnlage"

type AnlageDocument struct {
	DOLFDNR     int
	Title       string
	ContentType string
	Name        string
	SavedAt     time.Time
	Error       string
	Size        int
}
